<?php
// read in names of stations
$fp = fopen("./src/station_list.csv", 'r');
// first line contains column names, read away
$headers = fgetcsv($fp);
$names = array();

// read in all station names and add to names array
while($line = fgetcsv($fp))
{
    array_push($names, $line[0]);
}

// check for existing favourites
if(isset($_COOKIE['favourites'])) {
    $favs = $_COOKIE['favourites'];
    $favs = explode(",", $favs);
}

?>
<body>
    <div class="row row-centered">
      <div class="jumbotron row-centered">
        <h1>Weather Wizard</h1>
        <p>The weather you want.</p>
      </div>
    </div>

    <div class="row row-centered" style="margin:0px 0px;">
        <div class="col-md-12">
            <h2>Where are you?</h2>
            <small>(or where do you want to be?)</small>
        </div>
    </div>
        <div class='row row-centered'>
            <div id="city-in" role="input">
                <?php
                $capital = array("Melbourne",
                                 "Sydney",
                                 "Canberra",
                                 "Hobart",
                                 "Brisbane",
                                 "Adelaide",
                                 "Perth",
                                 "Darwin");
                //Close enough is good enough
                $capStns = array("Moorabbin Airport",
                                  "Goulburn",
                                  "Bombala AWS",
                                  "Warra",
                                  "Gladstone Airport",
                                  "Adelaide Airport",
                                  "Mandurah",
                                  "Cape Don");
                $i=0;
                // echo whatever input buttons are called
                echo "<div class='row-centered row'>";
                for($i;$i<count($capital);$i++)
                {
                    // echo other capital cities as input buttons
                    echo "<div class ='col-md-1 capitals'>";
                    echo "<button class='capitals' onclick='clicker(this);' value='" . $capStns[$i] . "'>" . $capital[$i] . "</button>";
                    // echo whatever the buttons are called
                    echo "</div>";
                }
                echo "</div>"
                 ?>
                 <p><small>or perhaps somewhere more specific...</small></p>
                <select name="city" onchange="clicker(this);">
                    <option value = '' disabled selected>Select a station</option>"
                    <optgroup label="Your Favourites">
                        <?php
                        if($favs){
                            foreach($favs as $fav)
                            {
                                echo "<option value='$fav'>$fav</option>";
                            }
                        }
                        else {
                            echo "<option value ='' disabled>Add favourites to see them here!</option>";
                        }
                         ?>
                    </optgroup>

                    <optgroup label="All Stations">
                        <?php
                        foreach($names as $sname)
                        {
                            echo "<option value ='$sname'>$sname</option>";
                        }
                        ?>
                    </optgroup>
                </select>
            </div>
        </div>
    </div>
    <!-- include custom js -->
    <script type='text/javascript'>
    function clicker(sel) {
        window.location.assign(window.location.href + '?station=' + sel.value);
    }
    </script>
</body>
