from unittest import TestCase
from DataReader import DataReader

class TestDataReader(TestCase):
    def setUp(self):
        self.filename = "../src/station_list.csv"
        self.dr = DataReader(self.filename)


    def test_read_station_file_information(self):
        f = open(self.filename, "r")
        count = len(f.readlines()) - 1 #-1 for the header line which is ignored in the datareader class
        self.assertEquals(len(self.dr.station_line), count)

    def test_get_station_line(self):
        self.assertEquals(self.dr.get_station_line(), self.dr.station_line)

    def tearDown(self):
        pass