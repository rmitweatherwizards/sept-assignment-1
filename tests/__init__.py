import unittest
from test_weatherStation import TestWeatherStation
from test_dataReader import TestDataReader

if __name__ == "__main__":
    #unittest.main()
    suite1 = unittest.TestLoader().loadTestsFromTestCase(TestWeatherStation)
    suite2 = unittest.TestLoader().loadTestsFromTestCase(TestDataReader)

    allTests = unittest.TestSuite([suite1, suite2])

    unittest.TextTestRunner(verbosity=2).run(allTests)