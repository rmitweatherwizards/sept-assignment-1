var plotData = {};
plotData.time = {};
plotData.temperature = {};
plotData.dewPoint = {};
plotData.windSpeed = {};
plotData.apparentTemperature = {};

//Source: https://github.com/FVANCOP/ChartNew.js/issues/310
// plotData.forecastLine = {};

// generate chart on page load
$(document).ready(function() {
    // generate table
    populateTable('#table');
    // magic table
    $('#table').DataTable();
    /* * * Graphing things * * */
    // get canvas context
    var ctx = $("#chart");
    var i = 1;
    // var colours = {"",'rgba(r,g,b,a)','rgba(r,g,b,a)','rgba(r,g,b,a)','rgba(r,g,b,a)'};
    var atts = Object.keys(plotData);
    // massage data into correct format
    var data = {
        labels: plotData.time.data, // x axis labels
        //labels: [5,10,15,20],
        datasets: [
            {
              //Temp
                label: plotData[atts[i]].name,
                data: plotData[atts[i++]].data,
                backgroundColor: "rgba(214,119,92,0.4)",
                borderColor: "rgba(214,119,92,0.4)",
                fill: false
            },
            {
              //Dew point
                label: plotData[atts[i]].name,
                backgroundColor: "rgba(192, 217, 175, 0.4)",
                borderColor: "rgba(192, 217, 175, 0.4)",
                data: plotData[atts[i++]].data,
                fill: false
            },
            {
              //windspeed
                label: plotData[atts[i]].name,
                backgroundColor: "rgba(200,118,227,0.2)",
                borderColor: "rgba(200,118,227,0.2)",
                data: plotData[atts[i++]].data,
                fill: false
            },
            {
              //apparent temp
                label: plotData[atts[i]].name,
                backgroundColor: "rgba(118,200,227,0.4)",
                borderColor: "rgba(118,200,227,0.4)",
                data: plotData[atts[i++]].data,
                fill: false
            }//,
            // {
            //   pointColor : "rgba(0,0,0,0)",
            //   strokeColor : "red",
            //   pointStrokeColor : "rgba(0,0,0,0)",
            //   xPos : plotData.forecastLine.data,
            //   data : [0,40],
            //   label: plotData.forecastLine.name
            // }
        ]
    }

    chartOptions = {}

    // generate chart
    var myChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {}
    });
});

function populateTable(id)
{

    plotData.time.data = stationdata["time"].concat(stationdata['fc_time']);
    plotData.time.name = "Timestamp";
    plotData.temperature.data = stationdata["temperature"].concat(stationdata['fc_temperature']);
    plotData.temperature.name = "Temperature (degrees Celsius)";
    plotData.dewPoint.data = stationdata["dewPoint"].concat(stationdata['fc_dewPoint']);
    plotData.dewPoint.name = "Dew Point (degrees Celsius)";
    plotData.windSpeed.data = stationdata["windSpeed"].concat(stationdata['fc_windSpeed']);
    plotData.windSpeed.name = "Windspeed (km/hr)";
    plotData.apparentTemperature.data = stationdata["apparentTemperature"].concat(stationdata['fc_apparentTemperature']);
    plotData.apparentTemperature.name = "Apparent Temperature (degrees Celsius)";

    //For drawing vertical line
    // plotData.forecastLine.name = "Forecast";
    // plotData.forecastLine.data = stationdata["time"].concat(stationdata['fc_time']); //not entirely sure how objects work in jQuery
    //
    // var firstForecastTime = stationdata['fc_time'][0];
    // for (var i = 0; i < plotData.forecastLine.data.length; i++){
    //     if (plotData.forecastLine.data[i] == firstForecastTime){
    //       i++;
    //     }
    //     else {
    //       plotData.forecastLine.data[i] = 0;
    //     }
    // }

    var atts = Object.keys(plotData);

    for(var i = 0;i<atts.length;i++){
        $(id + ' thead tr').append('<th>' + atts[i] + '</th>');
    }

    for(var i = 0;i<plotData.time.data.length;i++)
    {
        $(id + ' tbody').append("<tr> </tr>");
        for(var j = 0;j<atts.length;j++)
        {
            $(id + ' tbody tr:last').append("<td>" + plotData[atts[j]].data[i] + "</td>");
        }
    }
}
/* sample cookie handling code provided by W3C */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function toggleCookie(station) {
    var favs = getCookie("favourites");
    if (favs == "") {
        setCookie("favourites",station, 30);
    } else {
        // if the station isn't already in the cookie
        if((favs.indexOf(station) == -1))
        {
            // add it to cookies
            str = favs + station + '-';
            setCookie("favourites",(str),30);
            $('#fav-success').show().delay(2000).fadeOut(300);
            $('#fav-btn').html("Remove from favourites");
        } else {
            setCookie("favourites",(favs.replace((station + '-'), "")), 30);
            setCookie("favourites",(favs.replace((station), "")), 30);
            $('#fav-warning').show().delay(2000).fadeOut(300);
            $('#fav-btn').html("Add to favourites");
        }

    }
}

function checkCookie(station) {
    var favs = getCookie("favourites");
    if (favs == "") {
        return;
    } else {
        // if the station isn't already in the cookie
        if((favs.indexOf(station) == -1))
        {
            return;
        } else {
            $('#fav-btn').html("Remove from favourites");

        }

    }
}
