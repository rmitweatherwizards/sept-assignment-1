<!DOCTYPE html>
<head>
    <title>Weather Wizard</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Google Raleway Font (fresh af) -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- dataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/bs-3.3.6/jq-2.2.3,dt-1.10.12,fh-3.1.2,r-2.1.0/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/u/bs-3.3.6/jq-2.2.3,dt-1.10.12,fh-3.1.2,r-2.1.0/datatables.min.js"></script>
    <!-- Bootstrap style -->
    <link href="https://bootswatch.com/superhero/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!-- chart.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.bundle.min.js"></script>
    <!-- custom css -->
    <link href="./styles/main.css" rel="stylesheet">



</head>

<?php

if(isset($_GET['station']))
{
    include('weather.inc');
}
else {
    include('home.inc');
}


?>
