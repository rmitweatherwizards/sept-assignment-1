# README #

### About ###

Repository for Software Engineering: Process & Tools Major Assessment.

This application uses a python daemon (made mostly of assignment 1 code) running on a web server in order to maintain data in a constant state. Web requests call on data from this daemon and generate a webpage as such. 


### Setup ###

* All that is required is a web browser! go to [n0t.online/sept/](http://n0t.online/sept/) to use the application.


### Contribution guidelines ###

* document your code
* follow python programming conventions (snake_case, etc)


### Tools ###
* [Trello board](https://trello.com/b/DnPOn7d4) - project planning
* [Bitbucket repository](https://bitbucket.org/rmitweatherwizards/sept-assignment-1) - for version control
* [Slack Chat](https://weatherwizards.slack.com/) - communication tool
* Google Docs - test cases prep

### Contacts ###
* Peter Kydas, s3485580, 40% - main underlying code for stations, data handling, data retrieval, some Front-end work
* Charlie Edmonston, s3428865, 60% - Front-end web development, 'Daemonisation' of old python code, communication from Daemon to Web front end.

* Tutor: Amirhomayoon Ashrafzadeh, amirhomayoon.ashrafzadeh@rmit.edu.au
* Class Time: Wednesday, 8:30-10:30