#############################################
#   Title: StationMaster                    #
#   Desc: Keeps control of all the stations #
#############################################

from DataReader import DataReader
from WeatherStation import WeatherStation

import sys, os

class StationMaster:

    def __init__(self, filename):
        self.filename = filename
        self.stations = {}
        self.dr = DataReader(filename).get_station_line()

        self.init_stations()

    def init_stations(self):
        for s in self.dr:
            st = WeatherStation(s)
            temp_dict = {st.get_name(): st}
            self.stations.update(temp_dict)

    def get_station(self, name):
        return self.stations[name]

    def update_station(self, station_name):
        if self.stations[station_name]:
            self.stations[station_name].update()

    def get_stations(self):
        return self.stations

    def update_all_stations(self):
        no_info = []
        count = 0
        print "Getting station information"
        print "Information downloaded for stations: "
        for st in self.stations:
            #st = WeatherStation(s)
            self.stations[st].update()
            if self.stations[st].has_data():
                sys.stdout.write(st + ', ')
                count += 1
                if count > 5:
                    print
                    count = 0
            else:
                no_info.append(st )
            #temp_dict = {st.get_name(): st}
            #self.stations.update(temp_dict)

        sys.stdout.write("\nCould not get information for: ")
        for n in no_info:
            sys.stdout.write(n + ", ")
        print

    def get_states(self):
        self.states = []
        for station in self.stations:
            if (station[1] not in self.states):
                self.states.append(station[1])
        # print sys._getframe().f_code.co_name
        return self.states

    # returns a list of indices where each weather station pertaining to that state is
    def get_stations_per_state(self, state):
        pos = []
        for i in range(0, len(self.stations)):
            if self.stations.get_state() == state:
                pos.append(i)

        return pos

    def get_names(self):
        names = []
        for s in self.stations:
            names.append(s)

        return sorted(names)


