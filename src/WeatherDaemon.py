from Daemon import Daemon
from StationMaster import StationMaster
import os, socket, json, logging

class WeatherDaemon(Daemon):

    def __init__(self, station_fn, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        #logging.basicConfig(filename="/var/www/html/sept/src/weather.log", level=logging.DEBUG)
        Daemon.__init__(self, pidfile, stdin, stdout, stderr)
        # self.sockfile = "/var/www/html/sept/src/weatherwiz.sock"
        self.addr = "127.0.0.1"
        self.port = 50000
        self.sm = StationMaster(station_fn)

    """ daemon main loop"""
    def run(self):
        EOC = "///"

        # listen on socket
        #self.s.listen(5)

        # loop forever
        while(True):
            data = ''
            client, info = self.s.accept()
            # logging.info("Connected on %s:%d" % info)

            # wait until receives data
            while True:
                raw_data = client.recv(1024)
                data += raw_data
                if EOC in raw_data:
                    break

            """ data processing """
            ## sanitise data
            data = data[0:(data.find(EOC))]
            data = data.split(',');
            station = str(''.join(data[1:]))

            """ update station and get info """
            self.sm.update_station(station)
            atts = self.sm.get_station(station).get_all_graph_atts_names()
            station_data = {}
            for att in atts:
                station_data[att] = self.sm.get_station(station).get_all_graph_atts(att)

            #station_data['date'] = self.sm.get_station(station).get_time_s_full()
            
            station_data = json.dumps(station_data)

            ## return station data
            client.sendall(station_data + EOC)



            client.close()

        self.s.close()

    def stop(self):
        # try:
        #     self.s.close()
        # except socket.error, msg:
        #     logging.warning("Can't close socket: %s" % msg)
        Daemon.stop(self)

    def start(self):
        # create and bind socket
        try:
            print socket.getaddrinfo(self.addr, self.port)

            HOST = None                # Symbolic name meaning the local host
            PORT = 50000              # Arbitrary non-privileged port
            self.s = None
            for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
                af, socktype, proto, canonname, sa = res
                try:
                    self.s = socket.socket(af, socktype, proto)
                except socket.error, msg:
                    logging.error('Socket Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                    self.s = None
                    continue
                try:
                    self.s.bind(sa)
                    logging.info("Bind Complete")
                    self.s.listen(5)
                    logging.info("Now Listening to socket")
                except socket.error, msg:
                    logging.error('Socket bind/listening Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                    self.s.close()
                    self.s = None
                    continue
                break

            #self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #self.s.bind((self.addr, self.port))
        except socket.error as e:
            print e.strerror
            print "DAEMON ALREADY RUNNING??????"

        Daemon.start(self)

    def process_request(self, data):
        pass
