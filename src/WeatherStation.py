#########################################################################################
# Title: WeatherStation                                                                 #
# Desc: A module that contains all the functions to retrieve, store, and format         #
#       the data from the BoM site. Includes making HTTP connections and writing files  #
# Date: 21/3/16                                                                         #
#########################################################################################

from urllib2 import urlopen
import json
import sys
import re
import datetime

import logging
<<<<<<< HEAD

=======
>>>>>>> daemon

class WeatherStation:

    null_replacement = None # @TODO: fix this so that the missing space comes up in the graph but ggplot doesn't attempt to plot
    block_commands = None
    block_forecast_commands = None

    def __init__(self, file_line):
        r = re.compile('.*,.*,.*,.*,.*')
        if (r.match(file_line) == None):
            print ("Not a valid data line")
            return None

        l = file_line.split(',')
        self.name = l[0]
        self.state = l[1]
        self.lat = l[2]
        self.long = l[3]
        self.json_url = l[4]

<<<<<<< HEAD
        self.info = None

=======
        self.data = None

        # Relating to getting forecast data
        self.forecast_data = None
        self.forecast_api_key = "9252c7a5db8b3a07876d93556e474b52"
        # Full url: https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE
        self.forecast_base_url = "https://api.forecast.io/forecast/"

        # Converts attributes to be more readable and consistent
        self.att_convert = {'swell_period': 'swellPeriod', 'air_temp': 'temperature', 'wind_spd_kmh': 'windSpeed',
                            'apparent_t': 'apparentTemperature',  'press_msl': 'pressure', 'dewpt': 'dewPoint'}
>>>>>>> daemon
        self.g_atts = None
        # List of attributes that are common across historical data and forecast data
        self.graphable_attribute_list = ['temperature', 'apparentTemperature', 'windSpeed', 'time', 'dewPoint',
                                         'fc_temperature', 'fc_apparentTemperature', 'fc_windSpeed', 'fc_time', 'fc_dewPoint']

        self.time_local_length = 14

########################################################################################################################

    ### Various Data Functions ###
    def update(self):
        self.update_main_data()
        self.update_forecast()

    def update_main_data(self):
        attempts = 3  # retries 3 times if the URL fails
        while attempts:
            try:
<<<<<<< HEAD
                self.info = json.loads(urlopen(self.json_url).read())
=======
                self.data = json.loads(urlopen(self.json_url).read())
>>>>>>> daemon
                print ("Station updated successfully")
                self.block_commands = False
                break;

            except Exception:
                # import traceback
                # print ('Exception: ' + traceback.format_exc())
                attempts -= 1
                logging.info("Attempts for station %s is now %d", self.name, attempts)

        if attempts == 0:
            print ("Cannot get information for " + self.name)
            logging.warning("Cannot get information for %s", self.name)
<<<<<<< HEAD
            self.info = None
=======
            self.data = None
>>>>>>> daemon
            self.block_commands = True
            return
        # self.block_commands = self.has_data()
        self.clean_data()

    def clean_data(self):
        if self.block_commands:
            return None
        try:
            column_names = self.get_attributes()
            for data in self.data['observations']['data']:
                for c in column_names:
                    if data[c] is None:
                        data[c] = self.null_replacement
        except IndexError:
            print "Can't get column names to clean the data"
            logging.warning("Cannot get index error for %s. Possible problem with block_commands?", self.name)

<<<<<<< HEAD
=======
        self.data['observations']['data'] = list(reversed(self.data['observations']['data']))
        self.add_time_data()
        self.clean_attribute_names()


    #Function that gets the forecast io data for use in graphing
    def update_forecast(self):
        attempts = 3 #retries 3 times if the URL fails
        url = self.forecast_base_url + self.forecast_api_key + "/" + self.lat + "," + self.long
        while attempts:
            try:
                self.forecast_data = json.loads(urlopen(url).read())
                print ("Station forecast updated successfully")
                self.block_forecast_commands = False
                break;

            except Exception:
                attempts -= 1
                logging.info("Attempts for station [FORECAST] %s is now %d", self.name, attempts)

        if attempts == 0:
            print ("Cannot get forecast information for " + self.name)
            logging.warning("Cannot get [FORECAST] information for %s", self.name)
            self.forecast_data = None
            self.block_forecast_commands = True
            return

        self.clean_forecast_data()

    #Converts temperatures to celcius and changes time to be consistent with rest of data
    def clean_forecast_data(self):
        #currently, hourly, daily
        #Cleaning currently
        for c in self.forecast_data['currently']:
            c.encode('ascii', 'ignore')
            if 'temperature' in c or 'Temperature' in c:
                self.forecast_data['currently'][c] = self.fahr_to_cel(self.forecast_data['currently'][c])
            if 'time' in c or 'Time' in c:
                self.forecast_data['currently'][c] = self.time_convert(self.forecast_data['currently'][c])
            if 'windSpeed' in c:
                self.forecast_data['currently'][c] = self.mph_to_kph(self.forecast_data['currently'][c])
            if 'dewPoint' in c:
                self.forecast_data['currently'][c] = self.fahr_to_cel(self.forecast_data['currently'][c])


        #Cleaning hourly
        for h in self.forecast_data['hourly']['data']:
            for h2 in h:
                h2.encode('ascii','ignore')
                if 'temperature' in h2 or 'Temperature' in h2:
                    h[h2] = self.fahr_to_cel(h[h2])
                if 'time' in h2 or 'Time' in h2:
                    h[h2] = self.time_convert(h[h2])
                if 'windSpeed' in h2:
                    h[h2] = self.mph_to_kph(h[h2])
                if 'dewPoint' in h2:
                    h[h2] = self.fahr_to_cel(h[h2])


        #Cleaning daily
        for d in self.forecast_data['daily']['data']:
            for d2 in d:
                d2.encode('ascii', 'ignore')
                if 'temperature' in d2 or 'Temperature' in d2:
                    d[d2] = self.fahr_to_cel(d[d2])
                if 'time' in d2 or 'Time' in d2:
                    d[d2] = self.time_convert(d[d2])
                if 'windSpeed' in d2:
                    d[d2] = self.mph_to_kph(d[d2])
                if 'dewPoint' in d2:
                    d[d2] = self.fahr_to_cel(d[d2])


    #Converts UNIX time of forecast IO into the same time format as the other data
    def time_convert(self, time):
        return datetime.datetime.fromtimestamp(int(time)).strftime('%H:%M %d/%m/%Y')

    #Converts value from fahrenheit to celcius
    def fahr_to_cel(self, temp):
        return ((temp - 32) * (5.0/9.0))

    def mph_to_kph(self, speed):
        return (0.621371192 * speed)

########################################################################################################################
    #Misc getter functions
>>>>>>> daemon
    def get_name(self):
        return self.name

    def get_id(self):
        return self.json_url.strip('.json').split('/')[-1]

    def get_state(self):
        return self.state

    def has_data(self):
<<<<<<< HEAD
        if (self.info == None or len(self.info['observations']['data']) == 0):
=======
        if (self.data == None or len(self.data['observations']['data']) == 0):
>>>>>>> daemon
            return False
        else:
            return True

########################################################################################################################
    ### Attribute Data Handling ###

    def clean_attribute_names(self):
        for column in self.data['observations']['data']:
            for name in column:
                if name in self.att_convert:
                    column[self.att_convert[name]] = column.pop(name)


    def get_all_graph_atts_names(self):
        if self.block_commands or self.block_forecast_commands:
            logging.warning("Trying to get all graphable attributes when there's no data!")
            return None

        return self.graphable_attribute_list

    def get_all_graph_atts(self, att):
        if att.startswith('fc_'):
            att = att.strip('fc_')
            return self.get_attribute_data_forecast(att)
        else:
            return self.get_attribute_data(att)


    def get_attributes(self):
        if self.block_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
            return None

        return self.data['observations']['data'][0].keys()

    def get_graphable_attributes(self):
        if self.block_commands:
            return None

        atts = self.get_attributes()
        self.g_atts = []

        for data in self.data['observations']['data']:
            for a in atts:
                if isinstance(data[a], (int, float)):
                    self.g_atts.append(a)
                    atts.remove(a)

        return self.g_atts

    def get_attribute_data(self, att):
        if self.block_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
<<<<<<< HEAD
=======
            return None

        att_data = []
        for data in self.data['observations']['data']:
            att_data.append(data[att])
        return att_data

    def get_attribute_data_forecast(self, att):
        if self.block_forecast_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
>>>>>>> daemon
            return None

        att_data = []
        for data in self.forecast_data['hourly']['data']:
            att_data.append(data[att])
        return att_data

    def get_attribute_avg(self, att):
        if self.block_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
            return None

        data = self.get_attribute_data(att)
        avg_val = 0.0
        count = 0
        for d in data:
            try:
                if d != self.null_replacement:
                    val = float(d)
                    avg_val += val
                    count += 1
            except ValueError:
                print "Not a variable that can be averaged"
                return

        avg_val = avg_val/ count
        return avg_val


    def get_attribute_min(self, att):
        if self.block_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
            return None

        data = self.get_attribute_data(att)
        min_val = None
        for d in data:
            try:
                if d != self.null_replacement:
                    val = float(d)
                    if min_val is None:
                        min_val = val
                    elif val < min_val:
                        min_val = val
            except ValueError:
                return
        return min_val

    def get_attribute_max(self, att):
        if self.block_commands:
            logging.warning("Trying to get data when the station hasn't been updated!")
            return None

        data = self.get_attribute_data(att)
        max_val = None
        for d in data:
            try:
                if d != self.null_replacement:
                    val = float(d)
                    if max_val is None:
                        max_val = val
                    elif val > max_val:
                        max_val = val
            except ValueError:
                return
        return max_val

    def add_time_data(self):
        if self.block_commands:
            return None

        for d in self.data['observations']['data']:
            for c in d:
                if c == 'local_date_time_full':
                    d['time'] = self.parse_local_time_full(d[c])
                    break

    def get_time_s_full(self):
        # is returned in descending order, includes date
        if self.block_commands:
            return None

        data = self.get_attribute_data("local_date_time_full")
        times = []

        for d in data:
            times.append(self.parse_local_time_full(d))

        return times

    def get_time_s_min(self):
        # is returned in descending order, does not include data
        if self.block_commands:
            return None

        data = self.get_attribute_data("local_date_time_full")
        times = []

        for d in data:
            times.append(self.parse_local_time_min(d))

        return times

    def parse_local_time_full(self, l_time):
        if l_time is not None:
            l_time.encode('ascii', 'ignore')
            if len(l_time) != self.time_local_length:
                return None
            else:
                new_time = "%s:%s %s/%s/%s" % (l_time[8:10], l_time[10:12], l_time[6:8], l_time[4:6], l_time[0:4],)
                return new_time

    def parse_local_time_min(self, l_time):
        if l_time is not None:
            l_time.encode('ascii', 'ignore')
            if len(l_time) != self.time_local_length:
                return None
            else:
                new_time = "%s-%s:%s" % (l_time[6:8], l_time[8:10],l_time[10:12])
                return new_time

########################################################################################################################
    ### Print Functions ###

    def sprint(self):
        s = "Name: " + self.name +"\n"
        s += "ID: " + self.get_id() + "\n"
        s += "State: " + self.state
        return s

    def sprint_att(self, att):
        s = "Att: " + att + "\n"
        s += "Max: " + str(self.get_attribute_max(att)) + "\n"
        s += "Min: " + str(self.get_attribute_min(att)) + "\n"
        s += "Avg: " + str(self.get_attribute_avg(att))

        return s

    def _print(words):
        #words = station.split(',')
        print ("Name: " + words[0] + "\nState: " + words[1] + "\nID: " + words[4].split('/')[-1].strip('.json'))

    def print_data_one(self):
        if self.block_commands:
            return None

        print self.data['observations']

    def print_table(self):
        if self.block_commands:
            return None

        s_form = "{0: >%d}"
        space_offset = 5

        headers = self.data['observations']['header'][0].keys()

        # print header information for station
        for h in reversed(headers):
            print (h + ": " + self.data['observations']['header'][0][h])

        print

        column_names = self.data['observations']['data'][0].keys()
        column_names.remove("name") #unnecessary and clogs up table

        s_form_local_time = s_form % (len('local_date_time') + space_offset)
        sys.stdout.write(s_form_local_time.format('local_date_time|'))
        for c in column_names:
            s_form_tmp = s_form % (len(c) + space_offset)
            sys.stdout.write(s_form_tmp.format(c+"|"))
        print("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")

        # tabulate and print data information
        for data in self.data['observations']['data']:
            sys.stdout.write(s_form_local_time.format(str(data['local_date_time']+"|")))
            for c in column_names:
                if data[c] is None:
                    data[c] = "-"
                s_form_tmp = s_form % (len(c) + space_offset)
                sys.stdout.write(s_form_tmp.format(str(data[c])+"|"))
            print

    def sprint_table(self):
        if self.block_commands:
            return None

        s_form = "{0: >%d}"
        space_offset = 7
        s_out = ""

        try:
            headers = self.data['observations']['header'][0].keys()

        except AttributeError:
            return None

        # print header information for station
        for h in reversed(headers):
            s_out += (h + ": " + self.data['observations']['header'][0][h])

        s_out += "\n"

        column_names = self.data['observations']['data'][0].keys()
        column_names.remove("name") #unnecessary and clogs up table

        s_form_local_time = s_form % (len('local_date_time') + space_offset)
        s_out += (s_form_local_time.format('local_date_time|'))
        for c in column_names:
            s_form_tmp = s_form % (len(c) + space_offset)
            s_out = (s_form_tmp.format(c+"|"))
        s_out += ("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n")

        # tabulate and print data information
        for data in self.data['observations']['data']:
            s_out += (s_form_local_time.format(str(data['local_date_time']+"|")))
            for c in column_names:
                if (data[c] == None):
                    data[c] = "-"
                s_form_tmp = s_form % (len(c) + space_offset)
                s_out += (s_form_tmp.format(str(data[c])+"|"))
            s_out += "\n"

        return s_out
