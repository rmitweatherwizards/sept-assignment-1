############################ Application.py ###################################
#   Desc: GUI Interface to WeatherStation.py - weather data visualiser        #
#   Date: 12/3/16                                                             #
#   Authors: Peter Kydas                                                      #
#            Charles Edmonston                                                #
###############################################################################

import Tkinter as tk
import ttk
import pandas as pd
import matplotlib as mpl

# For graphing in tkinter
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from ggplot import *


class GUIApp(tk.Frame):
    def __init__(self, s, master=None):
        tk.Frame.__init__(self, master, bg="green") #may make this root? separates variables
        self.sm = s
        self.grid(sticky="nsew")
        # self.pack(fill="both", expand=True)

        self.config_filename = "window.cfg"
        self.configInfo = {"x_pos": 0, "y_pos": 0, "height": 0, "width": 0}

        self.favourites_filename = "favourites.txt"
        self.fav_stations = []
        self.fav_station_count = 2

        self.selected_station = None # cursor selection for station
        self.selected_att_plot = None # cursor selection for data type
        # self.valid_station_bool = False

        if self.read_config():
            self.set_config()

        self.read_fav()

        self.create_widgets()

        self.set_dpi()


    """
    Main function to draw GUI
    """
    def set_dpi(self):
        # needed for resizing plots
        # taken from:
        width_px = self.winfo_screenwidth()
        height_px = self.winfo_screenheight()
        width_mm = self.winfo_screenmmwidth()
        height_mm = self.winfo_screenmmheight()
        # 2.54 cm = in
        self.width_in = width_mm / 25.4
        self.height_in = height_mm / 25.4
        self.width_dpi = width_px/self.width_in
        self.height_dpi = height_px/self.height_in

        print('Width: %i px, Height: %i px' % (width_px, height_px))
        print('Width: %i mm, Height: %i mm' % (width_mm, height_mm))
        print('Width: %f in, Height: %f in' % (self.width_in, self.height_in))
        print('Width: %f dpi, Height: %f dpi' % (self.width_dpi, self.height_dpi))

    def create_widgets(self):

        # top level window definitions and params
        top = self.winfo_toplevel()

        top.rowconfigure(0, weight=1)
        #top.rowconfigure(1, weight=1)
        top.columnconfigure(0, weight=0)
        top.columnconfigure(1, weight=1)
        top.columnconfigure(4, weight=0)
        top.minsize(400, 400)

        # layout definitions
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=0)

        self.left_frame = tk.Frame(top)
        self.left_frame.rowconfigure(0, weight=1)
        self.left_frame.grid(row=0, column=0, sticky="news")

        # create ListBox for stations
        self.create_listbox(self.left_frame)
        self.populate_listbox()

        # attempt to create treeview
        # self.create_tree()

        # create label for showing data
        self.create_graph_window(top)

        self.create_misc_window(top)

        # frame for buttons
        self.button_frame = tk.Frame(self.left_frame)
        #self.button_frame.rowconfigure(0, weight=1)
        # create go go_button
        self.go_button = tk.Button(self.button_frame, text='Select', command=self.go_butt_func)
        self.go_button.grid(row=0, column=0, sticky="news")


        # create quitButton go_button
        self.quitButton = tk.Button(self.button_frame, text='Quit', command=self._quit)
        self.quitButton.grid(row=0, column=1, sticky="news")

        #self.button_frame.grid(row=3, column=0, sticky="nesw")
        self.button_frame.grid(row=3, column=0)

        # setup weighting for gui
        """
        self.grid_columnconfigure(1, weight=0)
        self.grid_columnconfigure(0, weight=0)
        """

    def update(self):
        # main update function when something gets selected_station
        #self.update_graph_window()
        self.update_notebook()


    """
    ListBox initialisation and config
    """

    def create_listbox(self, master=None):
        # define frame to hold listbox
        self.lbframe = tk.Frame(master, relief=tk.SUNKEN, bg="blue")

        # define scrollbar
        self.yScroll = tk.Scrollbar(self.lbframe, orient=tk.VERTICAL)
        self.yScroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.xScroll = tk.Scrollbar(self.lbframe, orient=tk.HORIZONTAL)
        self.xScroll.pack(side=tk.BOTTOM, fill=tk.X)

        # initialise listbox
        self.lb = tk.Listbox(self.lbframe, yscrollcommand=self.yScroll.set, xscrollcommand=self.xScroll.set)
        self.lb.pack(fill=tk.BOTH, expand=1)
        # self.lb.pack()
        # self.lb.grid(row=0, column=0, sticky=tk.N + tk.S + tk.W + tk.E)
        # self.lbframe.grid(row=1, column=0, rowspan=1, columnspan=1, sticky=tk.N+tk.S+tk.W+tk.E)
        self.lbframe.grid(row=0, column=0, rowspan=3, columnspan=1, sticky="news")
        # self.lbframe.grid(row=1, column=0, rowspan=2, columnspan=2, sticky=tk.N+tk.E+tk.S+tk.W, padx=5, pady=5)

        self.yScroll.config(command=self.lb.yview)
        self.xScroll.config(command=self.lb.xview)

    def populate_listbox(self):
        # for all stations in stationmaster
        #   self.lb.insert(END,station)
        #

        self.update_fav()
        self.lb.insert(tk.END, "Stations")
        self.lb.insert(tk.END, "===============")
        stations = self.sm.get_names()
        for s in stations:
            self.lb.insert(tk.END, s)

    """
    Label graph window and config
    """
    def create_graph_window(self, master=None):
        self.graph_frame = tk.Frame(master, bg="purple")

        """
        self.textBoxLabel = tk.StringVar() #every time this var is changed, the label is also updated
        self.main_view = tk.Label(self.graph_frame, textvariable=self.textBoxLabel, fg="green", bg="black", anchor=tk.W, justify=tk.LEFT)
        # self.main_view = tk.Canvas(self.main_view_frame)
        self.textBoxLabel.set("Please select a state")
        """
        self.graph_frame.grid(row=0, column=1, columnspan=2, rowspan=2, sticky="nesw")

        #self.main_view.grid(row=0, column=0, sticky="nesw")
        # self.main_view.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

        """
        # # y define scrollbar
        self.y_main_scroll = tk.Scrollbar(self.main_view_frame, orient=tk.VERTICAL)
        self.y_main_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.y_main_scroll.config(command=self.main_view.yview)

        # # x define scrollbar
        self.x_main_scroll = tk.Scrollbar(self.main_view_frame, orient=tk.HORIZONTAL)
        self.x_main_scroll.pack(side=tk.BOTTOM, fill=tk.X)
        self.x_main_scroll.config(command=self.main_view.xview)
        """

    def update_graph_window(self):
        #self.textBoxLabel.set(self.sm.stations[self.selected_station].sprint())
        #self.textBoxLabel.set(self.sm.stations[self.selected_station].sprint())
        label = self.sm.get_station(self.selected_station).sprint_table();
        if label is not None:
            self.textBoxLabel.set(self.sm.get_station(self.selected_station).sprint_table())
        else:
            self.textBoxLabel.set("Data could not be retrieved")



    """
    Other frame for misc functions and whatnot
    """
    def create_misc_window(self, master=None):
        self.misc_window = tk.Frame(master, bg="yellow")

        self.misc_window.grid(row=0, column=2, rowspan=3, columnspan=2, sticky="nesw")

        self.misc_window.columnconfigure(0, weight=1)
        self.misc_window.rowconfigure(0, weight=1)

        self.create_notebook(self.misc_window)

    def create_notebook(self, master=None):
        self.nb = ttk.Notebook(master)
        self.page1 = ttk.Frame(self.nb)
        self.page2 = ttk.Frame(self.nb)

        self.nb.add(self.page1, text="Attributes")
        self.nb.add(self.page2, text="Options")

        ## Attributes Tab ##
        # Info Block 1 #
        self.page1_text1 = tk.StringVar()
        self.page1_label1 = tk.Label(self.page1, textvariable=self.page1_text1, fg="green", bg="black", anchor=tk.W, justify=tk.LEFT)
        # self.main_view = tk.Canvas(self.main_view_frame)
        self.page1_label1.pack(fill=tk.BOTH, expand=0)
        self.page1_text1.set("\nPlease select a state\n")

        # Info Block 2 #
        self.page1_text2 = tk.StringVar()
        self.page1_label2 = tk.Label(self.page1, textvariable=self.page1_text2, fg="green", bg="black", anchor=tk.W, justify=tk.LEFT)
        # self.main_view = tk.Canvas(self.main_view_frame)
        self.page1_label2.pack(fill=tk.BOTH, expand=0)
        self.page1_text2.set("\nNo Data Currently Selected\n\n")

        # Attributes list box #

        #Scroll bar
        self.yScroll_p1_lb = tk.Scrollbar(self.page1, orient=tk.VERTICAL)
        self.yScroll_p1_lb.pack(side=tk.RIGHT, fill=tk.Y)

        # create listbox #
        self.page1_lb = tk.Listbox(self.page1, yscrollcommand=self.yScroll_p1_lb.set)
        self.page1_lb.pack(fill=tk.BOTH, expand=1)
        #self.page1_lb.grid(row=0, column=0)

        self.yScroll_p1_lb.config(command=self.page1_lb.yview)
        # end attributes list box #

        # Attribute buttons #
        self.att_button_grid = tk.Frame(self.page1)
        self.att_button_plot = tk.Button(self.att_button_grid, text="Plot!", command=self.att_plot)
        self.att_button_plot.grid(row=0, column=0, sticky="news")
        self.att_button_data = tk.Button(self.att_button_grid, text="Data", command=self.att_show_data)
        self.att_button_data.grid(row=0, column=1, sticky="news")


        self.att_button_back = tk.Button(self.att_button_grid, text="Back", command=self.att_back)
        # self.att_button_grid.grid(row=1, column=0, sticky="news")
        self.att_button_grid.pack(fill=tk.Y, expand=0)
        # End Buttons #
        # End Attributes Tab #

        ## Options tab ##
        self.options_button_grid = tk.Frame(self.page2)
        self.options_fav = tk.Button(self.options_button_grid, text="Favourite", command=self.opt_fav).grid(row=0, column=0, sticky="news")
        self.options_refresh = tk.Button(self.options_button_grid, text="Refresh", command=self.opt_refresh).grid(row=1, column=0, sticky="news")
        self.options_button_grid.grid(row=0, column=0, sticky="news")
        #.options_button_grid.grid(row=0, column=0)
        # End options tab #

        self.nb.grid(row=0, column=0, columnspan=2,  sticky="news")

    def update_notebook(self):
        if self.is_valid_name():
            # assume station has already been updated
            s = self.sm.get_station(self.selected_station)
            self.page1_text1.set(s.sprint())

            if s.has_data():
                attributes = self.sm.get_station(self.selected_station).get_graphable_attributes()

                self.page1_lb.delete(0, tk.END)

                if attributes is not None:
                    for a in attributes:
                        self.page1_lb.insert(tk.END, a.encode('ascii', 'ignore'))



    """
    Create TreeView for displaying tabular data
    """

    def create_tree(self):
        self.tree_frame = tk.Frame(self, bg="blue")
        self.tree = ttk.Treeview(self.tree_frame)
        self.tree['columns'] = ('size', 'modified', 'owner')

        # Inserted at the root, program chooses id:
        self.tree.insert('', 0, 'name', text='Station Name')

        # Same thing, but inserted as first child:
        self.tree.insert('name', 'end', 'gallery', text='random shit')

        # Treeview chooses the id:
        id = self.tree.insert('', 'end', text='Tutorial')

        # # Inserted underneath an existing node:
        # self.tree.insert('widgets', 'end', text='Canvas')
        # self.tree.insert(id, 'end', text='Tree')
        #
        # self.tree['columns'] = ('size','date','modified')
        # self.tree.set('widgets', 'size', '12KB')
        # size = self.tree.set('widgets', 'size')
        # self.tree.insert('', 'end', text='Listbox', values=('15KB Yesterday mark'))
        #
        self.tree.grid(row=0, column=0, columnspan=3, sticky=tk.N + tk.S + tk.E + tk.W)
        self.tree_frame.grid(row=1, column=1, rowspan=1, columnspan=3, sticky=tk.N + tk.S + tk.E + tk.W)


        # # y define scrollbar
        # self.y_tree_scroll = tk.Scrollbar(self.tree_frame, orient=tk.VERTICAL)
        # self.y_tree_scroll.pack(side=tk.RIGHT,fill=tk.Y)
        # self.y_tree_scroll.config(command=self.tree.yview)
        #
        # # x define scrollbar
        # self.x_tree_scroll = tk.Scrollbar(self.tree, orient=tk.HORIZONTAL)
        # self.x_tree_scroll.pack(side=tk.BOTTOM,fill=tk.X)
        # self.x_tree_scroll.config(command=self.tree.xview)

    def update_tree(self):
        self.tree.move('widgets', 'gallery', 'end')  # move widgets under gallery
        self.tree.delete('widgets') #removes node permanently
        self.tree.detach('widgets') #does not delete, can be put back with move

        self.tree = ttk.Treeview(None, columns=('size', 'modified'))

        self.tree.column('size', width=100, anchor='center')
        self.tree.heading('size', text='Size')
        pass

    """
    Maintains window position and configuration
    """
    def set_config(self):
        print "Setting config"
        geo = "%dx%d+%d+%d" % (self.configInfo["width"], self.configInfo["height"], self.configInfo["x_pos"], self.configInfo["y_pos"])
        self.master.geometry(geo)
        print geo

    def get_config(self):
        self.configInfo["x_pos"] = self.winfo_toplevel().winfo_x()
        self.configInfo["y_pos"] = self.winfo_toplevel().winfo_y()
        self.configInfo["height"] = self.winfo_toplevel().winfo_height()
        self.configInfo["width"] = self.winfo_toplevel().winfo_width()

    def write_config(self):
        writer = open(self.config_filename, "w+")
        self.get_config()
        for c in self.configInfo:
            writer.write(c + ":" + str(self.configInfo[c]) + "\n")
        print "Written to file"

    def read_config(self): #if reading successful, returns true
        try:
            f = open(self.config_filename, "r")
        except IOError:
            print "No config file"
            return False

        for line in f:
            l = line.split(":")
            self.configInfo[l[0]] = int(l[1])
        return True

    """
    Functions relating to maintaining of Favourites
    """
    def read_fav(self):
        try:
            f = open(self.favourites_filename, "r")
        except IOError:
            print "No favourites specified"
            return False

        for line in f:
            self.fav_stations.append(self.sm.get_station(line.strip("\n")).get_name())

        print "Favourites read in successfully."

    def write_fav(self):
        writer = open(self.favourites_filename, "w")
        if self.fav_stations[0] is not None:
            for s in self.fav_stations:
                writer.write(s)
                writer.write("\n")
        print "Written to favourites"

    def update_fav(self):
            self.lb.insert(0, "Favourites")
            self.lb.insert(1, "************************")
            if self.fav_stations != None:
                if self.fav_station_count > 2:
                    self.lb.delete(2, self.fav_station_count+2)
                    self.fav_station_count = 2
                for f in self.fav_stations:
                    self.lb.insert(self.fav_station_count, f)
                    self.fav_station_count += 1

            self.lb.insert(self.fav_station_count, " ")

    # KeyError checking + if nothing selected_station && updates station if successful
    def is_valid_name(self):
        if self.selected_station is not None:
            try:
                self.sm.get_stations()[self.selected_station]
                return True
            except (KeyError, AttributeError):
                print "Not a valid station"
                return False
        else:
            return False


    """
    Button input handling
    """
    # method for handling press of submit go_button
    def go_butt_func(self):
        try:
            self.selected_station = self.lb.get(self.lb.curselection())
            if self.is_valid_name():
                # Updates in the above function
                self.sm.update_station(self.selected_station)
                self.update()
        except tk.TclError:
            print ("Nothing selected!")

    # method for closing the window
    def _quit(self):
        # window shit: http://stackoverflow.com/questions/14910858/how-to-specify-where-a-tkinter-window-opens
        self.write_config()
        self.write_fav()

        # destroy any graphs before closing
        plot_children = self.graph_frame.winfo_children()
        for child in plot_children:
            child.destroy()

        #plt.close('all')

        self.destroy() #deletes window
        self.quit()

    ### Attributes ###
    def att_plot(self):
        if self.att_get_cursor_selection():

            children = self.graph_frame.winfo_children()

            for child in children:
               child.destroy()

            att_data = self.sm.get_station(self.selected_station).get_attribute_data(self.selected_att_plot)
            #att_time = self.sm.get_station(self.selected_station).get_time_s_full()
            att_time = self.sm.get_station(self.selected_station).get_time_s_min()

            self.page1_text2.set(self.sm.get_station(self.selected_station).sprint_att(self.selected_att_plot))

            data = {'att': att_data,'time': range(0,(len(att_time)))}

            df = pd.DataFrame(data=data)

            x_spacing_default = 8 # default: 10

            if len(data) > x_spacing_default/2:
                x_spacing = 1
            else:
                x_spacing = x_spacing_default


            # mpl.rcParams["figure.figsize"] = "3, 2"
            # automatically drops NA
            gg = ggplot(df, aes('time', 'att')) + \
                geom_point() + geom_smooth(alpha=0.2) + \
                scale_x_discrete(breaks=range(0,(len(att_time)),x_spacing),labels=att_time) + \
                xlab("Time") + ylab(self.selected_att_plot) + \
                ggtitle("Recent " + self.selected_att_plot + " Data")+ \
                theme(axis_text_x=element_text(angle=45, hjust=1))

            g = gg.draw()
            # gg.close()
            # my_dpi = 96.0
            w = self.graph_frame.winfo_width()
            h = self.graph_frame.winfo_height()

            #g.set_size_inches(100/my_dpi, 100/my_dpi, forward=True)
            #g.set_size_inches(2,2)
            #g.set_dpi(my_dpi)

            canvas = FigureCanvasTkAgg(g, self.graph_frame)
            canvas.show()

            tk_canvas = canvas.get_tk_widget()
            # can have offset in here
            tk_canvas.config(width=w, height=h) # @TODO: time label getting cut off because of ggplot, canvas is displaying correctly
            tk_canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)


            #plt.close()

            print "Added plot"


    def att_show_data(self):
        if self.att_get_cursor_selection():
            data = self.sm.get_station(self.selected_station).get_attribute_data(self.selected_att_plot)
            self.page1_text2.set(self.sm.get_station(self.selected_station).sprint_att(self.selected_att_plot))
            # erase all the data in the listbox and add attribute data
            self.page1_lb.delete(0, tk.END)

            for d in data:
                self.page1_lb.insert(tk.END, str(d))

            self.page1_lb.configure(state=tk.DISABLED)

            # set up all the buttons
            self.att_button_data.grid_forget()
            self.att_button_back.grid(row=0, column=1, sticky="news")


    def att_get_cursor_selection(self):
        # assume everything in that listbox is a valid selection
        try:
            self.selected_att_plot = self.page1_lb.get(self.page1_lb.curselection())
            return True
        except tk.TclError:
            print "Nothing selected in attributes window"
            return False

    def att_back(self):
        self.page1_lb.configure(state=tk.NORMAL)
        self.update_notebook()
        self.att_button_back.grid_forget()
        self.att_button_data.grid(row=0, column=1, sticky="news")

    ### Options ###
    def opt_fav(self):
        if self.is_valid_name():
            name = self.sm.get_station(self.selected_station).get_name()
            if name in self.fav_stations:
                self.fav_stations.remove(name)
                print "Removed " + name + " from favourites."
            else:
                self.fav_stations.append(name)
                print "Added " + name + " to favourites."
        self.update_fav()

    def opt_refresh(self):
        if self.is_valid_name():
            self.sm.update_station(self.selected_station)
            self.update()
        else:
            print "Could not refresh"

    # end GUIApp class