##############################WeatherWizard.py###############################
#   Title: WeatherWizard                                                    #
#   Desc: Main program execution                                            #
#   Date: 23/3/16                                                           #
#   Authors:    Charles Edmonston                                           #
#               Peter Kydas                                                 #
#############################################################################

from StationMaster import StationMaster
import logging
from WeatherDaemon import WeatherDaemon
import getopt, sys

def main(argv):

    stationfile = "station_list.csv"
<<<<<<< HEAD
    FORMAT = '%(levelname)-9s %(asctime)-15s %(message)s'
    logging.basicConfig(filename='weather.log', level=logging.DEBUG, format=FORMAT)
    logging.info("Setting filename to %s", stationfile)

    wd = WeatherDaemon(stationfile, '/tmp/SM.pid')
=======
    FORMAT = '%(filename)-18s %(levelname)-9s %(asctime)-15s %(message)s'
    logging.basicConfig(filename='weather.log', level=logging.DEBUG, format=FORMAT)
    logging.info("Setting filename to %s", stationfile)

    wd = WeatherDaemon(stationfile, '/var/www/html/sept/src/WD.pid')
>>>>>>> daemon

    try:
        opts, args = getopt.getopt(argv, "hstr", ["help", "start", "terminate", "restart"])
    except getopt.GetoptError:
        print 'usage: StationMaster.py --start|--terminate|--restart (-s|-t|-r)'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('h', "--help"):
            print 'usage: StationMaster.py --start|--terminate|--restart (-s|-t|-r)'
            sys.exit()
        elif opt in ("s", "--start"):
            wd.sm.init_stations()
            wd.sm.update_station("Adelaide Airport")
            print "StationMaster Daemon initiated"
            wd.start()
        elif opt in ("t", "--terminate"):
            wd.stop()
        elif opt in ("r", "--restart"):
            wd.restart()



if __name__ == "__main__":
    main(sys.argv[1:])





#
# def main():
#     station_filename = "station_list.csv"
#     """
#     sm = StationMaster(station_filename)
#     s = sm.get_station("Albany Airport")
#     s.update()
#     at = s.get_graphable_attributes
#     t = s.get_time_s_full()
#     t2 = s.get_time_s_min()
#     for i in range(0, len(t)):
#         print t[i] + "\t" + t2[i]
#     """
