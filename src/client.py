#!/usr/bin/python
"""
client.py
to be wrapped up snugly in php
"""

import sys, socket, logging
from DataReader import DataReader

def main(argv):
    EOC = "///"
    host = "127.0.0.1"
    port = 50000
    tosend = str(' '.join(argv)) + EOC


    FORMAT = '.:%(levelname)-9s:. %(filename)s %(asctime)-15s %(message)s'
    logging.basicConfig(filename='weather.log', level=logging.DEBUG, format=FORMAT)

    filename = "/var/www/html/sept/src/station_list.csv"
    dr = DataReader(filename).get_station_line()

    names = []
    for d in dr:
        names.append(d.split(',')[0])

    if (tosend.split(",")[1][0:-3]) not in names:
        # s.close()
        print "error"
        sys.exit(2)
        
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((host,port))


    logging.info("sending: " + tosend)
    s.sendall(tosend)

    data = ''
    while True:
        logging.info("client.py: being recv from " + host + ":" + str(port))
        raw_data = s.recv(1024)
        data += raw_data
        if EOC in raw_data:
            break

    # clean data
    data = data[0:(data.find(EOC))]

    #close socket when done
    s.close()
    # print program output
    print data
if __name__ == '__main__':
    main(sys.argv[1:])
