<?php
// get selected station name for output rendering
$station = $_GET['station'];
// define values for retrieving information
$python = "sudo /usr/bin/python";
$script = "{$_SERVER['DOCUMENT_ROOT']}/sept/src/client.py";
// use comma as argument seperator
$sep = ',';
// construct argument sting from station name and seperator
$args = "get". $sep .  $station;

$cmd = escapeshellcmd("$python $script $args");

$output = shell_exec($cmd);

if(strcmp(trim($output),"error") == 0)
{
    $cmd = $python . "{$_SERVER['DOCUMENT_ROOT']}/sept/src/WeatherWizard.py --t";
    shell_exec($cmd);
    sleep(3);
    $cmd = $python . "{$_SERVER['DOCUMENT_ROOT']}/sept/src/WeatherWizard.py --s";
    shell_exec($cmd);
    echo "<script>alert('You shouldn't be here? How'd you do that???');</script>";
    header('Location: http://n0t.online/sept/');
    exit;
}
 ?>


<body>
    <div class='row row-centered'>
        <div style="float:left; padding: 2em 2em;">
            <button onclick="window.location.assign('/sept/')">GO BACK</button>
        </div>
        <div class='col-md-8 col-centered'>
            <h1>Weather for <?php echo $station; ?></h1>
            <button id='fav-btn' class='favourite' style="margin:auto auto 2em auto;" onclick="toggleCookie('<?php echo $station; ?>');">Add to favourites</button>
            <div id='fav-success' class="alert alert-success" onclick='$(this).fadeOut(500);'>
                <p><?php echo $station; ?> has been added to favourites!</p>
            </div>
            <div id='fav-warning' class="alert alert-warning" onclick='$(this).fadeOut(500);'>
                <p><?php echo $station; ?> has been removed from favourites!</p>
            </div>
        </div>
    </div>
    <div id='content' class='row row-centered'>
        <div style="margin-bottom:2em;" class='col-md-8 chart col-centered'>
            <div>
                <canvas id="chart" width="400" height="300"></canvas>
            </div>
        </div>
    </div> <!-- row -->
    <div class='row row-centered'>
        <div class='col-md-9 col-centered'>
            
            <table id='table'>
                <thead>
                    <tr>

                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div> <!-- row -->



    <?php
    $output = json_encode($output);
    echo "<script type='text/javascript'>var stationdata = JSON.parse($output); </script>";
    ?>

    <!-- include custom js -->
    <script type='text/javascript' src="./scripts/main.js"></script>
    <?php echo "<script type='text/javascript'> checkCookie('$station'); </script>"; ?>
</body>
